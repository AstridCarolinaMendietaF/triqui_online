function color(){
  document.body.style.background = "#999";
}

let bandera =false;                                                  //INDICA SI EL JUEGO INICIA
let turno = 0;                                                       //DETERMINA EL TURNO
let tab = new Array();                                               //ARREGLO DE BOTONES 
let gan1; 
 
window.onload = function(){                                          //FUNCION PARA INICIAR EN EL BOTON COMENZAR
  let iniciar = document.getElementById("iniciar");
  iniciar.addEventListener("click",comenzar);
}
 
function comenzar(){                                  
  bandera = true;                                                    //SE INICIA EL JUEGO
  let jug1 = document.getElementById("jug1");                        //SE GUARDAN LOS NOMBRES DE LOS JUGADORES
  let jug2 = document.getElementById("jug2");

  if(jug1.value == ""){                                              //SE VALIDA EL NOMBRE J1 y J2
    alert("ATENCIÓN!!!! DIGITE UN NOMBRE PARA EL JUGADOR #1");        

    jug1.focus();                                                    
    }else{
      if(jug2.value == ""){
        alert("ATENCIÓN!!!! DIGITE UN NOMBRE PARA EL JUGADOR #2");    
        jug2.focus();
      }else{                                                          //SE ASIGNAN LOS BOTONES AL ARREGLO
        tab[0] = document.getElementById("b0");
        tab[1] = document.getElementById("b1");
        tab[2] = document.getElementById("b2");
        tab[3] = document.getElementById("b3");
        tab[4] = document.getElementById("b4");
        tab[5] = document.getElementById("b5");
        tab[6] = document.getElementById("b6");
        tab[7] = document.getElementById("b7");
        tab[8] = document.getElementById("b8");
        for(var i=0;i<9;i++){                                         //SE RECORREN LOS 9 BOTONES Y SE LE 
          tab[i].className= "botonInicial";                           //ASIGNAN LA NUEVA CLASE EN BLANCO
          tab[i].value = "I";
        }
        turno = 1;
        document.getElementById("turnoJ").innerHTML = "ADELANTE JUGADOR   " +jug1.value+"!!!!!!";
      }
   }
}

function colocar(boton){                                             //CAMBIO DE TURNO CON LA AYUDA DE LAS "I"
  if(bandera == true){
    if(turno == 1 && boton.value == "I"){
      turno = 2;
      boton.value = "X";
      boton.className = "botonJ1";                                   // CAMBIA EL ESTILO DEL BOTON A X
    }else{
      if(turno == 2 && boton.value == "I"){
        turno = 1;
        boton.value = "O";
        boton.className = "botonJ2";                                 // CAMBIA EL ESTILO DEL BOTON A Y
      }
    }
  }
  revisar();
  document.getElementById("premio").innerHTML = "EL VENCEDOR ES  " +gan1+"!!!!!!";
}

  function revisar(){                                                //MANERAS DE VICTORIA
    if((tab[0].value == "X" && tab[1].value == "X" && tab[2].value == "X")
    || (tab[3].value == "X" && tab[4].value == "X" && tab[5].value == "X")
    || (tab[6].value == "X" && tab[7].value == "X" && tab[8].value == "X")
    || (tab[0].value == "X" && tab[3].value == "X" && tab[6].value == "X")
    || (tab[1].value == "X" && tab[4].value == "X" && tab[7].value == "X")
    || (tab[2].value == "X" && tab[5].value == "X" && tab[8].value == "X")
    || (tab[0].value == "X" && tab[4].value == "X" && tab[8].value == "X")
    || (tab[2].value == "X" && tab[4].value == "X" && tab[6].value == "X"))
    {
    alert("GANADOR JUGADOR #1 "+jug1.value);
    gan1 =jug1.value;
    bandera = false;
    }else{

      if((tab[0].value == "O" && tab[1].value == "O" && tab[2].value == "O")
      || (tab[3].value == "O" && tab[4].value == "O" && tab[5].value == "O")
      || (tab[6].value == "O" && tab[7].value == "O" && tab[8].value == "O")
      || (tab[0].value == "O" && tab[3].value == "O" && tab[6].value == "O")
      || (tab[1].value == "O" && tab[4].value == "O" && tab[7].value == "O")
      || (tab[2].value == "O" && tab[5].value == "O" && tab[8].value == "O")
      || (tab[0].value == "O" && tab[4].value == "O" && tab[8].value == "O")
      || (tab[2].value == "O" && tab[4].value == "O" && tab[6].value == "O")
      ){
      alert("GANADOR JUGADOR #2 "+jug2.value);
      gan1 =jug2.value;
      bandera = false;}
    }
  }  

  
  
